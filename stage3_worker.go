package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/go-redis/redis"
	"log"
	"math"
	"os/exec"
	"regexp"
	"strconv"
	"time"
)

var client = redis.NewClient(&redis.Options{
	Addr:     "localhost:6379",
	Password: "", // no password set
	DB:       0,  // use default DB
})

func main() {

	//Connect to Redis
	var outb, errb bytes.Buffer
	matched := regexp.MustCompile(`frame=\s{0,}(\d+)`)

	for {

		var iter = 1
		for {
			if fname := readFromQueue(); fname != "" {

				//Get the count of frames
				cmd := exec.Command("ffmpeg",
					"-i", fname,
					"-map", "0:v:0",
					"-c", "copy",
					"-f", "null", "-")
				cmd.Stdout = &outb
				cmd.Stderr = &errb
				err := cmd.Run()
				if err != nil {
					log.Fatal(err)
				}

				res := matched.FindStringSubmatch(errb.String() + outb.String())
				if len(res) < 2 {
					log.Panic("Could not get frames count")
					continue
				}

				totalFrames, err := strconv.ParseInt(res[1], 10, 64)
				if err != nil {
					log.Panic(err)
					continue
				}

				//Run convertation
				log.Printf("[Iteration: %v] Convert %v", iter, fname)
				iter += 1
				cmd = exec.Command("ffmpeg", "-i", fname,
					"-c:v", "libx264",
					"-b:v", "1000k",
					"-c:a", "aac",
					"-f", "mp4", "outfile.mp4", "-y")
				stderrIn, _ := cmd.StderrPipe()

				if err := cmd.Start(); err != nil {
					log.Fatal(err)
				}

				var progress = int64(0)

				cmdOutBuf := ""
				oldProgress := int64(-1)
				oneByte := make([]byte, 1)
				for {

					_, err := stderrIn.Read(oneByte)
					if err != nil {
						fmt.Printf(err.Error())
						break
					}
					r := bufio.NewReader(stderrIn)
					line, _, _ := r.ReadLine()

					cmdOutBuf = cmdOutBuf + string(line)
					if matched.MatchString(cmdOutBuf) {
						tmp := matched.FindStringSubmatch(cmdOutBuf)
						if len(tmp) > 0 {
							tmp2, err := strconv.ParseInt(tmp[1], 10, 64)
							if err == nil {
								progress = tmp2
							}
						}
						cmdOutBuf = ""
					}
					currentProgress := float64(progress) * 10000 / float64(totalFrames)
					if oldProgress != progress {
						log.Printf("Progress: %v (%v frames of %v)", math.Round(currentProgress)/100, progress, totalFrames)
						oldProgress = progress
					}
					time.Sleep(time.Millisecond)

				}

				err = cmd.Wait()
			}

		}
	}
}

func readFromQueue() string {
	res := client.RPop("stage_worker3")
	if res.Err() != nil {
		return ""
	}
	return res.Val()
}
