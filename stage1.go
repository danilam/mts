package main

import (
	"log"
	"os/exec"
	"time"
)

const MAX_CHAN_SIZE = 10
const ITERATIONS = 10

var queue_chan = make(chan string, MAX_CHAN_SIZE)

func main() {
	go myRoutine()
	for i := 0; i < ITERATIONS; i++ {
		for len(queue_chan) == MAX_CHAN_SIZE {
			time.Sleep(2 * time.Millisecond)
		}
		queue_chan <- "./inputfile.avi"
	}

	for {
		if len(queue_chan) == 0 {
			break
		}
	}
}

func myRoutine() {
	var iter = 1
	for {
		select {
		case fname := <-queue_chan:
			{
				log.Printf("[Iteration: %v] Convert %v", iter, fname)
				iter += 1
				cmd := exec.Command("ffmpeg", "-i", fname,
					"-c:v", "libx264",
					"-b:v", "1000k",
					"-c:a", "aac",
					"-f", "mp4", "outfile.mp4", "-y")
				if err := cmd.Start(); err != nil {
					log.Fatal(err)
				}

				log.Printf("Waiting for command to finish...")
				err := cmd.Wait()

				log.Printf("Command finished with error: %v", err)
				time.Sleep(time.Second)
			}
		}
	}
}
