package main

import (
	"github.com/go-redis/redis"
	"log"
	"time"
)

func main() {
	//Connect to Redis
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	for {
		client.LPush("stage_worker3", "inputfile.avi")
		log.Println("New item added to queue")
		for client.LLen("stage_worker3").Val() > 10 {
			time.Sleep(time.Second)
		}
	}

}
